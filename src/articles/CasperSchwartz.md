![Casper Schwarz](https://ari.geenius.ee/app/uploads/sites/17/2022/03/644f466ceba4b6350203e91a516bf3d0-scaled-e1646220704542-856x482.jpg)

# Casper Schwarz: tuleb unustada juurdunud reeglid ja mõelda südamega

> Tunnustatud Hollandi arhitekt Casper Schwarz evitab uut kontseptsiooni, mis muudab kontori mõnusaks kohaks, kus pärast tööpäeva lõppu klaas veini juua või miks mitte ka juurvilju kasvatada. Pikemas usutluses räägib Schwartz pandeemia ajal sündinud julgest ideest, kõhklevatest hoiakutest ja koostööst maailma vallutava Eesti sisustusettevõttega OCCO.

#### Kuidas algas teie koostöö OCCOga ja miks see teid paelus?

Tutvusin OCCOga veidi üle aasta tagasi. Hakkasin disainima nende Amsterdami kontorit ja tahtsin tutvuda ka nende platvormiga, et jätkata koostööd ka tulevikus. Turul on mitu sarnast platvormi, kus disainerid saavad mööblit otsida ja valida, aga OCCO oma erineb selle poolest, et seal näed peale mööblivaliku kohe ka hindasid. 

Kujuta ette, et oled sisearhitekt ja sul on mõtteis mingi projekt. Valid lagesid, põrandaid ja mööblit, aga hinnad varieeruvad 500 eurost 5000 euroni. Teed oma valiku ära ja saad siis teada, et oled läinud kõvasti üle eelarve. OCCOga näedki kohe hinda ja lisaks saad valida eri firmade vahel. See on väga käepärane ja eriline võimalus. 

Sa ei taha ju kliente petta: et valid kenad elemendid välja, kuid pead neile ütlema, et hind tuleb kaks korda kallim. Nii hakkab tunduma, et kõik, mille arhitekt välja valib, peab olema väga kallis, aga nii see ei ole! See on täielik jaburus, sest tegelikult tahab arhitekt iga projekti jaoks sobiva toote leida. 

Teine hea omadus on see, et OCCO valikus on erinevaid tootjaid. Saab valida ka väiksemate või vähem tuntud tegijate seast. Vahel on just neil erakordselt äge mööbel. Meile meeldib aeg-ajalt enda mööblivalikut projektide kaupa muuta ja uusi asju proovida. OCCO on selles osas palju kaasa aidanud.

![Mõnus äraolemine OCCO Amsterdami kontori avaüritusel mullu sügisel. Foto: OCCO](https://ari.geenius.ee/app/uploads/sites/17/2022/03/0708b298371af345cf516529ff370028-1920x1280.jpg)

#### Olete loonud täiesti uue kontseptsiooni Habitoor. Miks seda vaja oli ja mida see endast kujutab?

Habitoori loomise ajendiks oli koroonapandeemia. See seadis uude valgusesse selle, milline peaks olema töökeskkond ja kontor. Aastate jooksul on kontor olnud peamiselt kohaks, kus palju inimesi saavad koos töötada. Nii on ehitatud või valitud ka enamik kontorihooneid. Arvestatakse eelkõige valguse, akustika, ruumikujunduse ja õhukonditsioneeriga. See on süstemaatiline kontseptsioon, mis on ehitatud eelkõige arvude järgi. 

Muutus, mille Habitoor tõi, seisneb selles, et kui inimestel on võimalus töötada mujal kui kontoris, siis peab olema põhjus, miks üldse kontorisse tulla. See ei ole seepärast, et nad peavad, vaid seetõttu, et nad tahavad. Sellise keskkonna loomisel peab mõtlema indiviidi seisukohalt. Esiteks, kas sulle meeldivad kolleegid, ja teiseks, milline on see ruum, kuhu tuled. Mida sinna jõudes tunned, mida seal teha saab, kas see koht sind inspireerib ja millist energiat pakub. 

Nii mõtlesime, et unustame tavalise kontori põhimõtted täiesti ära ja teeme hoopis teisiti. Habitoor on justkui organisatsiooni kodu. Kui mõelda kodule, siis ei ole seal ju neid elemente, mis on kontoris. Meie põhimõte on luua koht, kus oleks kolleegidega hea olla ja tööd teha, aga ka vaba aega nautida ja puhata. 

![Näide Habitoori kontseptsioonist.Foto: Casper Schwarz Architects](https://ari.geenius.ee/app/uploads/sites/17/2022/03/7bfc3ec28fa704f1c4aaa275662a2bcc-856x431.jpg)

#### Mis siis on kaasaegse töökeskkonna loomisel kõige tähtsam?

Tuleb unustada kõik tavaline ja mõelda südamega, inimese perspektiivist. Kui mõelda sellele, milline oleks sinu unistuste kontor, siis ma usun, et see ei näeks välja nagu kontor. Mõtle, kui äge oleks töötada kohas, kus tahad kogu aeg teha Instagrami selfisid või pilte enda päevast. Sa tunned end seal hästi ja võib-olla võite töökaaslastega pärast tööd veel aega veeta, juttu ajada ja klaasikese veini võtta.

#### Millised mööblielemendid peaks seal olema?

Üldiselt on mööblimaailmas kaks suunda: projekti- ja kodumööbel. Millegipärast on projektimööbel väga lihtsakoeline – tavaline kuju ja värvid, detaile eriti ei ole. Ma ei tea, miks see nii on. Samad inimesed istuvad seal diivanil või laua taga – miks peab projektilaud olema niivõrd teistsugune? Ma ei ütle, et kontoris peab olema ülimugav tugitool, aga kindlasti võiks olla kontorimööbel selline, mida ka oma koju tahaksid.

#### Kas Habitoori saab rakendada ükskõik kus või on siin ikkagi omad piirid?

Üks asi on see, et Habitoori saab rajada mistahes hoonesse. Näiteks võib teha kontori kasvõi talumajja – töötajad saavad õue minna, ise toitu kasvatada või metsas jalutada. Aga on veel palju teisi hoonetüüpe, kuhu kontor luua – kõik oleneb ikkagi firmast ja inimestest. 

Raskus seisneb aga selles, et kui mulle antakse tundmatu territoorium, siis peab täpselt läbi mõtlema, kuidas sinna kontor luua. Peab looma kontseptsiooni, mis lõpuks ikkagi töökeskkonnana töötaks. Arvestada tuleb ka seda, et kokku tuleb ilmselt mitu inimtüüpi, mis tähendab, et keskkond peab kõigile sobima. Mõnele on vaja vaikust, mõnele hoopis ruumi, kus loov olla. Kindlasti teeme Habitoori kontoreid rajades ka vigu, aga sellised väljakutsed on minu arust väga huvitavad ja neid on huvitav lahendada.

#### Kas see võib olla probleem, kui inimesed tunnevad end tööl liiga mugavalt?

Ma ei usu. Oma kodus tunneb inimene end ju väga hästi ja mugavalt. Kui mitte, siis on midagi halvasti. Kui ma räägin hollandlase pilgu läbi, siis ei ole ma veel kuskilt kuulnud, et kodukontoris inimesed nüüd enam tööd ei tee või järsku laisaks muutuvad. On isegi öeldud, et produktiivsus on pandeemia ajal kasvanud. Arvan, et see on vanamoodne mõtlemine, et mugavus produktiivsust pärsib. Me elame ambitsioonikas maailmas, kus inimesed tahavad oma tööd aina paremini teha. Mugavus pigem toetab ambitsiooni. 

![Visand Habitoori kontorilahendusest.Foto: Casper Schwarz Architects](https://ari.geenius.ee/app/uploads/sites/17/2022/03/3e86add28e7fc353967547f426199ec0-scaled.jpg)

#### Näete siis seda trendi, et klassikaline kontor on ajalugu või vähemalt muutumas?

Mitmed organisatsioonid tahavad muutust, nad otsivad selleks võimalusi ja inspiratsiooni. Soov on olemas. Samas kardavad nad muutusi. Oleme teinud Habitoori esitlusi mitmes ettevõttes ja inimesed on alati esialgu küll väga huvitatud, aga kui ma lõpuks küsin, et mis te ikkagi arvate, siis saan aru, et nad on siiski vanas mudelis kinni. Et tõkkeid murda, peab leidma ettevõtte, kes on selleks päriselt valmis ja saab aru, miks on muutus oluline. Praegu on olukord selline, et huvi leidub, aga julgust mitte. 

#### Mis seda barjääri murda aitaks – sõnumit rohkem kommunikeerida?

Jah, see on väga hea mõte. Enne pandeemiat ei olnud ka iga kontor tüüpiline ja igav koht. Mitmed idufirmad on enda talentidele ägedad ruumid loonud. Nemad ei taha töötada nagu vanemad suurkorporatsioonid. Ma näen ikkagi, et uued tegijad alustavad kõike, ka kontori loomist, hoopis uue vaatega. Selliseid projekte tuleb ka teistele rohkem esitleda ja näidata, mida lahedat teha saab. Nii astume sammu edasi. 

Meie teeme omalt poolt Habitoori esitlusi ja püüame neis alati minna sammu kaugemale – esitleme ka pisidetaile. Ehk saavad nii ka inimesed parema pildi, miks muutust vaja on ja kuidas vaadata hoonet ning töökeskkonda teise pilguga. Nii saab samm-sammult inimeste mõttemaailma muuta. 