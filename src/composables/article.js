export const articles = [
	{
		id: 1,
		date: '10.01.2022',
		featured: true,
		image: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1287&q=80',
		title: 'Architect interview: Casper Schwarz',
		description: 'Dutch architect says: forget the old rules and think with your heart. '	
	},
	{
		id: 2,
		date: '15.02.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1497366754035-f200968a6e72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2301&q=80',
		title: 'Design tips: Secret to a great home office space',
		description: 'As the pandemic changed our lifestyle, we need to also change our home style.'	
	},
	{
		id: 3,
		date: '10.01.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1585264550248-1778be3b6368?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1363&q=80',
		title: 'HAY: The Scandinavian success story',
		description: 'Established in 2002 HAY really made a difference in the Nordic furniture market.'	
	}
]