export const blog = [
	{
		id: 1,
		date: '10.01.2022',
		featured: true,
		image: 'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1287&q=80',
		title: 'Architect interview: Casper Schwarz',
		description: 'Dutch architect says: forget the old rules and think with your heart. '	
	},
	{
		id: 2,
		date: '15.02.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1497366754035-f200968a6e72?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2301&q=80',
		title: 'Design tips: Secret to a great home office space',
		description: 'As the pandemic changed our lifestyle, we need to also change our home style.'	
	},
	{
		id: 3,
		date: '10.01.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1585264550248-1778be3b6368?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1363&q=80',
		title: 'HAY: The Scandinavian success story',
		description: 'Established in 2002 HAY really made a difference in the Nordic furniture market.'	
	},
	{
		id: 4,
		date: '15.02.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1497215728101-856f4ea42174?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2670&q=80',
		title: 'Design tips: Secret to a great home office space',
		description: 'As the pandemic changed our lifestyle, we need to also change our home style.'	
	},
	{
		id: 5,
		date: '10.01.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1568992687947-868a62a9f521?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3432&q=80',
		title: 'HAY: The Scandinavian success story',
		description: 'Established in 2002 HAY really made a difference in the Nordic furniture market.'	
	},
	{
		id: 6,
		date: '15.02.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1541746972996-4e0b0f43e02a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2670&q=80',
		title: 'Design tips: Secret to a great home office space',
		description: 'As the pandemic changed our lifestyle, we need to also change our home style.'	
	},
	{
		id: 7,
		date: '10.01.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1564069114553-7215e1ff1890?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=3432&q=80',
		title: 'HAY: The Scandinavian success story',
		description: 'Established in 2002 HAY really made a difference in the Nordic furniture market.'	
	},
	{
		id: 8,
		date: '15.02.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1504384308090-c894fdcc538d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2670&q=80',
		title: 'Design tips: Secret to a great home office space',
		description: 'As the pandemic changed our lifestyle, we need to also change our home style.'	
	},
	{
		id: 9,
		date: '10.01.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1497366412874-3415097a27e7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2301&q=80',
		title: 'HAY: The Scandinavian success story',
		description: 'Established in 2002 HAY really made a difference in the Nordic furniture market.'	
	},
	{
		id: 10,
		date: '15.02.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1497366672149-e5e4b4d34eb3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2301&q=80',
		title: 'Design tips: Secret to a great home office space',
		description: 'As the pandemic changed our lifestyle, we need to also change our home style.'	
	},
	{
		id: 11,
		date: '10.01.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1572521165329-b197f9ea3da6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fG9mZmljZSUyMGludGVyaW9yJTIwZGVzaWdufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=900&q=60',
		title: 'HAY: The Scandinavian success story',
		description: 'Established in 2002 HAY really made a difference in the Nordic furniture market.'	
	},
	{
		id: 12,
		date: '15.02.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1488901512066-cd403111aeb2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzJ8fG9mZmljZSUyMGludGVyaW9yJTIwZGVzaWdufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=900&q=60',
		title: 'Design tips: Secret to a great home office space',
		description: 'As the pandemic changed our lifestyle, we need to also change our home style.'	
	},
	{
		id: 13,
		date: '10.01.2022',
		featured: false,
		image: 'https://images.unsplash.com/photo-1534134368327-3d2bd764f1ac?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MzV8fG9mZmljZSUyMGludGVyaW9yJTIwZGVzaWdufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=900&q=60',
		title: 'HAY: The Scandinavian success story',
		description: 'Established in 2002 HAY really made a difference in the Nordic furniture market.'	
	}
]