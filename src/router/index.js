import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/components/Home.vue'
import Blog from '@/components/Blog.vue'
import SingleArticle from '@/components/SingleArticle.vue'

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home
	},
	{
		path: '/blog',
		name: 'Blog',
		component: Blog
	},
	{
		path: '/single-article',
		name: 'Article',
		component: SingleArticle
	}
]

const router = createRouter({
	history: createWebHistory(),
	routes
})

export default router